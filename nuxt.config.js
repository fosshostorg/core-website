import pkg from "./package.json";

export default {
	server: {host: "0.0.0.0"},
	target: "static",
	srcDir: "src",
	buildModules: ["@nuxtjs/style-resources"],
	modules: ["@nuxt/content", "@nuxtjs/feed"],
	plugins: [{src: "@/plugins/matomo.js", mode: "client"}],
	css: ["normalize.css/normalize.css", "~style/index.scss"],
	components: [
		{path: "~/components"},
		{path: "~/components/news", prefix: "news"}
	],
	styleResources: {
		scss: ["~style/var.scss"]
	},
	content: {
		dir: "../content"
	},
	generate: {
		fallback: false
	},
	env: {
		description: pkg.description
	},
	head: {
		titleTemplate: (t) => (t ? `${t} - Fosshost` : "Fosshost"),
		meta: [
			{property: "og:image", content: "https://fosshost.org/preview.png"},
			{name: "viewport", content: "width=device-width, initial-scale=1"},
			{charset: "utf-8"}
		],
		link: [
			{
				title: "fosshost.org news",
				type: "application/rss",
				href: "/news/feed.xml",
				rel: "alternate"
			},
			{rel: "icon", type: "image/png", href: "/favicon.png"}
		]
	},
	build: {
		extend(config) {
			config.module.rules.push({
				loader: "ignore-loader",
				test: /\.md$/i
			});
		}
	},
	static: {
		cacheDir: ".cache"
	},
	feed: [
		{
			path: "/news/feed.xml",
			type: "rss2",
			async create(feed) {
				const {$content} = require("@nuxt/content");

				feed.options = {
					link: "https://fosshost.org/news",
					description: pkg.description,
					title: "Fosshost"
				};

				const posts = await $content("news", {deep: true})
					.where({draft: {$ne: true}})
					.sortBy("date", "desc")
					.fetch();

				posts.forEach((post) => {
					const url = `https://fosshost.org${post.dir}`;

					feed.addItem({
						date: new Date(post.date),
						description: post.summary,
						title: post.title,
						link: url,
						id: url
					});
				});
			}
		}
	]
};
