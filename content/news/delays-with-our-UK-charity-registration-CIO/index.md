---
title: Delays with our UK CIO charity registration 
date: 2021-03-01
summary: We are experiencing some delays with the Charity Comissioner 
draft: true
---

These are being worked on as a pressing issue, and we endeavour to provide a further update as soon as we can.
