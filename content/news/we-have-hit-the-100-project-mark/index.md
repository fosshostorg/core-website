---
title: We have hit the 100 projects mark!
date: 2021-03-01
summary: More than 100 open source projects are now benefitting from the services of Fosshost
---

<imgs src="100-projects.png"></imgs>

Fosshost is now providing cloud hosting services to more than 100 open-source software, initiatives and groups since our launch in April 2020. You can find a list of the projects we are helping [here](https://fosshost.org/projects). 

Fosshost CEO and Founder, Thomas Markey, said "This moment is a special day for Fosshost and confirms that our community model and approach is both sustainable and scalable. Further, it would not be possible without the support of our core sponsors."

Fosshost operates a community model whereby individuals or organisations donate hardware, services or professional services to the project.  Typically, Fosshost Volunteers will then apply our blueprint and take over full management and upkeep of the donation.  We provide regular updates to our [sponsors](https://fosshost.org/sponsors) on our progress and in return, offer sponsors marketing and PR opportunities for their support.  

Projects who [apply](https://fosshost.org/apply) for services with Fosshost will typically apply for our virtual private service (VPS) offering that is tailor-made depending on a project requirement.  This can be applied for via our website and we typically approve all applications within 72 hours.  Following approval, we then look to deliver the service shortly after. 

We ask projects to leave [testimonials](https://docs.fosshost.org/en/home/testimonials) after they have used our service to let other open-source projects know about their onboarding experience.

Watch for our next blog post where we will be providing plans for later this year.
