---
title: Update on sponsorships
date: 2020-11-08
summary: Fosshost relies upon sponsorships, and our Founder talks about how these sponsorships are critical to the work we do
---

Sponsorships are the cornerstone of fosshost.  Without them, we would not exist.  They underpin our mission to help every open source project to go further, together. Sponsorships help to bring sustainability to the open source community and provide access to stable hosting across a mixture of architectures and distributed cloud compute environments.  

Since the COVID-19 pandemic began, our services have never been in more need.  Larger and more established projects are receiving fewer donations that they were pre-COVID-19 and the rise of unemployment of software developers is forcing the community to consider how they spend their free cash.  Access to cloud computing services by commercial providers is an expense that cannot be ignored, or in some cases, considered at all.  These are scenarios where fosshost can help by removing those worries, without detriment to the user.

We have two different types of sponsorship.  Core hosting sponsors and project sponsors.

Core hosting sponsors typically provide dedicated hardware in their facility and provide power and connectivity. 

Project sponsors will typically provide transit, third-party services and software.

Every single supporter of fosshost is part of our supply chain and are all of equal importance.

[RapidSwitch.com](https://rapidSwitch.com) iomart Group PLC  
[Jolt.co.uk](https://jolt.co.uk) Host Lincoln Ltd  
[FDCservers.net](https://fdcservers.net) FDC Servers.net, LLC  
[HostKey.com](https://hostkey.com) HOSTKEY B.V. of Amsterdam  
[Serverius.net](https://serverius.net) Serverius B.V. of Amsterdam  
[DigitalOcean.com](https://digitalocean.com) DigitalOcean, Inc.  
[Equinix Metal](https://metal.equinix.com) Equinix, Inc.  
[OSUSOL.org](https://osuosl.org) Oregon State University Open Source Labs  
[CustodianDC.com](https://custodiandc.com) CustodianDC Ltd  

[Gandi.net](https://gandi.net) (Domain Registration Services)  
[Updown.io](https://updown.io) (Service Monitoring Tool)  
[Hund.io](https://hund.io) (Incident Management Tool)  
[Netlify.com](https://netlify.com) (Web Hosting)  
[DirectAdmin.com](https://directadmin.com) (Web Hosting)  


Occasionally, our supporters will write blog posts and newsletters to show their support.  Most recently, HostKey did just that and you can find more about their sponsorship and how it is helping by reading this great [article](https://www.hostkey.com/about/news/141).

If you are sponsoring fosshost and would like us to provide a case study for your company, please get in [touch](https://fosshost.org/contact).

