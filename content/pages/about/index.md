---
title: About
---

Fosshost C.I.C. is a non-profit UK-registered Community Interest Company (CIC), Limited by Guarantee, without share capital. Launched in April 2020 by founder and present CEO, Thomas Markey to provide open-source projects and initiatives access to no-cost distributed cloud computing services.

Today, several hundred open-source individuals and groups benefit from our global presence, which is only possible by donations from our sponsors and a team of highly talented and committed volunteers.

Many start-ups and large, established open-source projects benefit from our services, which in turn help to advance, promote and support the open-source community.  Overall, we are building a sustainable ecosystem to support the open-source community, which is growing daily.

We provide many other essential hosting services including website, email and domain name registration and management services to projects. 

Fosshost heavily relies upon donations from the general public and sponsors combined with our volunteers, to run the service. 


## Our network

The project operates a large and global infrastructure across multiple continents in world-class facilities.  Projects can select which region they want their services deploying on during the application process.  You can find information about the regions we operate from and the specifications of the virtualisation platforms we run within them on our wiki.  Occasionally, we run multiple nodes from the same facility. 


