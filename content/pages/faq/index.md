---
title: Frequently Asked Questions
---

**There is no such thing as free?**

Firstly, we are incredibly lucky and extremely grateful for the kind [donations](/donate) of infrastructure and networking made to the project, without these donations, we would simply not be able to operate.

We forge relationships with other projects who operate within the realms of free and open source and share our resources with them. These reciprocated morals of giving and sharing allow the project to operate at minimal cost. That being said, there is a lot of hard work which goes on behind the scenes to make the project successful.

**Why do you do this?**

We always felt the odd ones out because we couldn’t turn our hand to writing code or developing software, so instead we have pulled together to offer a service that is something we are good at. Open source isn’t just producing code, it is much more than that.

**How long will the project run for?**

We work hard to make sure that our donations are for the long term and not short term. We will continue to forge relationships with established providers who can kindly donate to the project, due to the very nature of the open source community being global, we are fortunate enough to operate in multiple continents with spare capacity, which means that we can operate with resilience.

**I am an individual and I want to learn Linux**

Sadly we are not able to provide our services directly to individuals wanting to learn Linux, as this would set a precedent and we do not have the bandwidth and processing power to be able to accommodate that. We are specifically here to help open source projects or groups. If you are an individual and involved with an open source project which may be eligible, consider applying via the project, as we may be able to approve your [application](/apply) in that regard.

**Is there any restrictions to the use of service?**

Technically, we do not block or restrict anything, but you must follow our [ToS](/legal#terms-of-service). If you are in breach of them, we reserve the right to shutdown your account, without notice. We do not allow our services to be used as exit nodes.

**I am a project and I would like to use you for mirrors?**

We support mirrors, please complete an [application](/apply).

**Do you offer dedicated hardware to my project?**

We support dedicated servers, subject to status and availablity, please complete an [application](/apply).

**I want to bring my own OS?**

Yes, you can select this during the [application](/apply).

**Do you offer monitoring?**

Yes, general monitoring is in place, as standard, however additional monitoring services are available upon request.

**Can I reboot my VM?**

Yes, go for it.

**Do you have bandwidth restrictions in place?**

No, however we do have to operate fair usage to avoid abuse and to ensure that all of our projects we help, get a fair share of the bandwidth. Anybody using obscene amounts of bandwidth, may be asked to stop.

**Do you accept donations?**

There are several ways you can donate, you can find ways to donate [here](/donate).

**I want to volunteer!**

Great, we'd love to have you onboard! Join us on our journey and [introduce](/contact) yourself. 

**I want a goody bag!**

Sure, just [contact us](/contact) with your mailing address, and we’ll ship one to you. Currently this includes a branded fosshost mug, pens, tshirt (please specify size) and notebook.

**Do you support IPv6?**

All US locations support IPv6.

**Where can I find your logos and artwork?**

Please see the [media](/media) page
