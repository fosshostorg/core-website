---
title: Sponsors
---

All of our infrastructure and networking is kindly donated and provided to the project for free. We could simply not operate without these contributions to the project and fosshost.org is extremely grateful for the kind generosity of all our supporters.

We believe in providing maximum transparency with our services and we are very proud to list those who have helped fosshost.org.  In no particular order.

## Core Hosting Sponsors

Core hosting sponsors typically donate infrastructure, hardware and networking services.  These donations are critical to our operation. 

[RapidSwitch.com](https://rapidswitch.com/), iomart Group PLC  
[Jolt.co.uk](https://jolt.co.uk/), Host Lincoln Ltd  
[FDCservers.net](https://fdcservers.net/), FDC Servers.net, LLC  
[HostKey.com](https://hostkey.com/), HOSTKEY B.V. of Amsterdam  
[Serverius.net](http://serverius.net/), Serverius B.V. of Amsterdam  
[DigitalOcean.com](https://digitalocean.com/), DigitalOcean, Inc  
[Equinix Metal](https://metal.equinix.com/), Equinix, Inc  
[OSUSOL.org](https://osuosl.org), Oregon State University Open Source Labs  
[Fastly.net](https://fastly.net), Fastly, Inc  

Fosshost works with many other sponsors which are listed on the respective project website, such as [AArch64.com](https://aarch64.com) and a special partnership with [Packetframe.com](https://packetframe.com)
