---
title: Contact
---

All support is provided by our volunteers and fellow users within our online community. We pride ourselves on offering an efficient and personal service to all of projects and users. To assist with this, we support a number of communication protocols to make our service accessible, wherever you are.

[Submit Ticket](https://support.fossho.st)  
[IRC](https://webchat.freenode.net/#fosshost)  
[Discord](https://discord.gg/8MfNdGK)  
[Matrix](https://matrix.to/#/#fosshost:matrix.org)  
[Telegram](https://t.me/fosshost)  
[Email](mailto:admin@fosshost.org)  

UK Emergency Telephone Number: +44 (0) 208 154 4278 (24/7 service available)


## Write to us

Fosshost C.I.C.  
7 Bell Yard  
London  
WC2A 2JR  
United Kingdom  

Fosshost is a Community Interest Company (CIC - prenounced 'kik') that provides charitable and non-profit services to the open source community.  We are registered in England and Wales.
