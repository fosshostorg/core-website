---
title: Donate
---

## We are now accepting donations

Many projects save significant costs and expenses by using our service but occasionally we incur emergency or unexpected costs.  By making a donation you help to keep the lights on.

We never ask for any financial reimbursement from those who we support, but if you are happy to donate any amount to help cover our running costs, it is very much appreciated.

<form action="https://www.paypal.com/donate" method="post" target="_top" style="margin-top: 8px;margin-left: 5px; ">
<input type="hidden" name="hosted_button_id" value="Y4R2W96V9VDJ6" />
<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1" />
</form>

</div>

## Other ways to donate

You could buy us a [coffee](https://ko-fi.com/fosshost)!

We are always looking for volunteers and we accept hardware or services, please email admin (at) fosshost.org
