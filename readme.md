## Development

```sh
# Install dependencies
$ npm i

# Run in development mode on http://localhost:3000
$ npm run dev
```

## Docker development

```sh
# Run in development mode on http://localhost:3000
$ docker-compose up -d
```

## Build

```sh
# Install dependencies
$ npm i

# Generate
$ npm run generate

# Deploy the generated dist folder
```

## Info

- Change site description in package.json
- Write markdown pages/posts in the content folder
- double click on a post in development mode to edit content in the browser

## Images in posts and pages

- Move image into _my_post/my_image.png_
- use `<imgs src="my_image.png"></imgs>` inside the markdown file
