FROM node:15-alpine

RUN npm i -g netlify-cli

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm i

COPY . .

RUN npm run generate
