module.exports = {
	bracketSpacing: false,
	arrowParens: "always",
	useTabs: true,
};
