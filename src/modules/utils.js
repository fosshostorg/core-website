export const date = (d) => {
	return new Date(d).toLocaleDateString("en", {
		year: "numeric",
		month: "short",
		day: "numeric"
	});
};
