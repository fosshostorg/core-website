import matomo from "vue-matomo";
import Vue from "vue";

export default ({app}) => {
	Vue.use(matomo, {
		host: "https://stats.webarch7.co.uk",
		router: app.router,
		siteId: 12
	});
};
